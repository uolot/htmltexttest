import re


class HtmlTextMixin(object):

    def assertHtmlText(self, html, lines, ignore_case=True):
        html_text_lines = convert(html, ignore_case)
        if ignore_case:
            lines = [l.lower() for l in lines]
        if not find_in_sequence(lines, html_text_lines):
            raise AssertionError("Lines %s not found in html document: %s..."
                                 % (lines, html[:100]))

    def assertNotHtmlText(self, html, lines, ignore_case=True):
        html_text_lines = convert(html, ignore_case)
        if ignore_case:
            lines = [l.lower() for l in lines]
        if find_in_sequence(lines, html_text_lines):
            raise AssertionError("Lines %s found in html document: %s..."
                                 % (lines, html[:100]))


def convert(html, ignore_case):
    """Replace all html tags with newlines, strip,
    and exclude empty lines

    """
    if ignore_case:
        html = html.lower()
    no_tags = re.sub(r"<[^>]+>", "\n", html)
    lines = [l.strip() for l in no_tags.split("\n")]
    return [l for l in lines if l]


# Following two functions come from: http://stackoverflow.com/a/3847690
# author: http://stackoverflow.com/users/192812/intuited


def reiterator(sub):
    """Yield elements of a sequence, resetting if sent ``True``."""
    it = iter(sub)
    while True:
        if (yield it.next()):
            it = iter(sub)


def find_in_sequence(sub, sequence):
    """Find a subsequence in a sequence.

    >>> find_in_sequence([2, 1], [-1, 0, 1, 2])
    False
    >>> find_in_sequence([-1, 1, 2], [-1, 0, 1, 2])
    False
    >>> find_in_sequence([0, 1, 2], [-1, 0, 1, 2])
    (1, 3)
    >>> find_in_sequence("subsequence",
    ...                  "This sequence contains a subsequence.")
    (25, 35)
    >>> find_in_sequence("subsequence", "This one doesn't.")
    False

    """
    start = None
    sub_items = reiterator(sub)
    sub_item = sub_items.next()
    for index, item in enumerate(sequence):
        if item == sub_item:
            if start is None:
                start = index
        else:
            start = None
        try:
            sub_item = sub_items.send(start is None)
        except StopIteration:
            # If the subsequence is depleted, we win!
            return (start, index)
    return False
