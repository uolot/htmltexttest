import unittest
from htmltexttest import HtmlTextMixin


HTML = """

<div>

    <h1>
        Text <span>foo <a href='#'>BAR</a></span>
    </h1>

</div>

<ul>
    <li>X1</li>
    <li>X2</li>
    <li>X3</li>
    <li>X4</li>
    <li>X5</li>
</ul>

"""


class HtmlTextMixinTestCase(unittest.TestCase):

    def test_assert_html_text(self):
        HtmlTextMixin().assertHtmlText(HTML, ["Text"])
        HtmlTextMixin().assertHtmlText(HTML, ["Text", "foo"])
        HtmlTextMixin().assertHtmlText(HTML, ["Text", "foo", "BAR"])
        HtmlTextMixin().assertHtmlText(HTML, ["X2", "X3", "X4"])

        HtmlTextMixin().assertHtmlText(HTML, ["text"], ignore_case=True)
        HtmlTextMixin().assertHtmlText(HTML, ["text", "foo"], ignore_case=True)
        HtmlTextMixin().assertHtmlText(HTML, ["text", "foo", "bar"], ignore_case=True)
        HtmlTextMixin().assertHtmlText(HTML, ["x2", "x3", "x4"], ignore_case=True)

        with self.assertRaises(AssertionError):
            HtmlTextMixin().assertHtmlText(HTML, ["Text2"])
        with self.assertRaises(AssertionError):
            HtmlTextMixin().assertHtmlText(HTML, ["Text", "foo", "bar"])
        with self.assertRaises(AssertionError):
            HtmlTextMixin().assertHtmlText(HTML, ["Text2"])
