# HtmlTextTest

Python's `TestCase` mixin that allows to test for a list of strings in HTML document, while ignoring all tags.


### Tests, instead of documentation:

    import unittest
    from htmltexttest import HtmlTextMixin


    HTML = """

    <div>
        <h1>
            Text <span>foo <a href='#'>BAR</a></span>
        </h1>
    </div>

    <ul>
        <li>X1</li>
        <li>X2</li>
        <li>X3</li>
        <li>X4</li>
        <li>X5</li>
    </ul>

    """


    class HtmlTextMixinTestCase(unittest.TestCase):

        def test_assert_html_text(self):
            HtmlTextMixin().assertHtmlText(["Text"], HTML)
            HtmlTextMixin().assertHtmlText(["Text", "foo"], HTML)
            HtmlTextMixin().assertHtmlText(["Text", "foo", "BAR"], HTML)
            HtmlTextMixin().assertHtmlText(["X2", "X3", "X4"], HTML)

            with self.assertRaises(AssertionError):
                HtmlTextMixin().assertHtmlText(["Text2"], HTML)
            with self.assertRaises(AssertionError):
                HtmlTextMixin().assertHtmlText(["Text", "foo", "bar"], HTML)
            with self.assertRaises(AssertionError):
                HtmlTextMixin().assertHtmlText(["Text2"], HTML)


### Run tests:

    $ pip install -r requirements.dev
    $ nosy -c nosy.cfg
